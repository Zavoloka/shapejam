// Fill out your copyright notice in the Description page of Project Settings.

#include "CharacterCup.h"
#include "Components/StaticMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "shapeCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "EngineUtils.h"

ACharacterCup::ACharacterCup()
{


	IsStaicMesh = false;

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMeshComponent->SetupAttachment(RootComponent);


}

void ACharacterCup::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{

	Super::SetupPlayerInputComponent(PlayerInputComponent);


	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &ACharacterCup::RestoreForm);



}


void ACharacterCup::SearachForActor()
{
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AshapeCharacter::StaticClass() , FoundActors);

	for (TActorIterator<AshapeCharacter> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		// Same as with the Object Iterator, access the subclass instance with the * or -> operators.
		RestorationActor = *ActorItr;
		break;	
	}
}

void ACharacterCup::RestoreForm()
{

	SearachForActor();
	if (RestorationActor) {

		UGameplayStatics::GetPlayerController(GetWorld(), 0)->Possess(RestorationActor);
	}

}