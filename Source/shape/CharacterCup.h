// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "shapeCharacter.h"
#include "CharacterCup.generated.h"

/**
 * 
 */
UCLASS()
class SHAPE_API ACharacterCup : public AshapeCharacter
{
	GENERATED_BODY()


private : 

	AshapeCharacter * RestorationActor;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ShapeComponents")
	bool IsStaicMesh;

	ACharacterCup();


	virtual void RestoreForm();
	

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ShapeComponents")
	UStaticMeshComponent* StaticMeshComponent;


	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


	virtual void SearachForActor();
	
};
